const express = require('express')
const app = express()
const http = require('http');

app.use(express.static(__dirname + ''));

app.get('*', function(req, res){
  res.sendFile(__dirname + '/src/public/index.html');
});

const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`App running on localhost:${port}`));