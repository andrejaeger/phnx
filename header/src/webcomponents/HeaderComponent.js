class HeaderComponent extends HTMLElement{
    
    constructor() {
        super();
        this._color = true;
    }

    get color(){
        return this._color
    }

    set color(val){
        this.setAttribute('color', element.style.marginTop);
    }

    static get observerdAttributes(){
        return ['color'];
    }   

    attributeChangedCallback(name, oldVal, newVal){
        switch(name){
            case 'color':
                this._color = false;
        }
    }

    connectedCallback(){
        var template = `

        <style>
          .header-component {
            text-align: center;
            border-style: dotted;
            border-color: blue;
          }
          
          .header-component-header {
            background-color: #222;
            height: 100px;
            padding: 20px;
            color: white;
          }
          
          .header-component-title {
            font-size: 1.5em;
          }
        </style>

        <div class="header-component">
            <header class="header-component-header">
                <h1 id='test' class="header-component-title">Welcome to OneAudi</h1>
            </header>
        </div>

        `; 

        this.innerHTML = template;
    }

}
window.customElements.define('header-component', HeaderComponent);

