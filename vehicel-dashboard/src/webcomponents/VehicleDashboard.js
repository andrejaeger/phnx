class VehicleDashboard extends HTMLElement{
    
    constructor() {
        super();
        //this.shadow = this.createShadowRoot();
        this._vehicle = '';
    }

    get color(){
        return this._vehicle
    }

    set color(val){
        this.setAttribute('vehicle', document.getElementById(test).innerHTML = val);
    }

    static get observerdAttributes(){
        return ['vehicle'];
    }   

    attributeChangedCallback(name, oldVal, newVal){
        switch(name){
            case 'vehicle':
                this._vehicle = 'red';
        }
    }

    connectedCallback(){
        var template = `

        <style>
          .vehicle-dashboard {
            text-align: center;
            border-style: dotted;
            border-color: red;
          }
          
          .vehicle-dashboard-form {
            background-color: gray;
            height: 400px;
            padding: 20px;
            color: black;
          }
          
          .vehicle-dashboard-title {
            font-size: 1.5em;
          }
        </style>

        <div class="vehicle-dashboard">
            <div class="vehicle-dashboard-form">
                <h1 class="vehicle-dashboard-title"> 2.Component </h1>
            </div>
        </div>

        `; 

        this.innerHTML = template;
    }

}
window.customElements.define('vehicle-dashboard-component', VehicleDashboard);