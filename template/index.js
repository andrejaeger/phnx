const express = require('express');
const app = express()
const http = require('http');
const esi = require('esi');


app.use(express.static(__dirname + ''), esi);

app.get('*', function(req, res){
  res.sendFile(__dirname + '/src/public/index.html');
});

const port = process.env.PORT || '8888';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`App running on localhost:${port}`));

